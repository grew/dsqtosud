
% =============================================================================================
% If an ellipsis is present in a conjunct, detect a head in the constituents of the conjunct and transform the DEP.COORD dependencies of the other constituents into an ORPHAN dependency from the head.
package coord_ellipsis {
  rule det_adj_cat {
    pattern {
      rel_coord: C1 -[coord|S:coord]-> GOV;
      DEP1[upos=ADJ]; DEP1 -[det|nmod:poss]-> *; GOV -[dep.coord]-> DEP1;
      depcoord_rel:GOV -[dep.coord|S:dep.coord]-> DEP2; DEP1 << DEP2;
    }
    without { GOV -[dep.coord]-> DEP; DEP << DEP1 }
    commands {
      del_edge depcoord_rel;
      add_edge DEP1 -[orphan]-> DEP2
    }
  }

  rule noun_cat {
    pattern {
      rel_coord: C1 -[coord|S:coord]-> GOV;
      DEP1[upos=NOUN|PRON|PROPN]; GOV -[dep.coord]-> DEP1;
      depcoord_rel:GOV -[dep.coord|S:dep.coord]-> DEP2; DEP1 << DEP2;
    }
    without { GOV -[dep.coord]-> DEP; DEP << DEP1 }
    commands {
      del_edge depcoord_rel;
      add_edge DEP1 -[orphan]-> DEP2
    }
  }

  rule cat_noun {
    pattern {
      rel_coord: C1 -[coord|S:coord]-> GOV;
      DEP1[upos=ADJ|NOUN|PRON|PROPN]; GOV -[dep.coord]-> DEP1;
      depcoord_rel:GOV -[dep.coord|S:dep.coord]-> DEP2; DEP1 >> DEP2;
    }
    without { DEP2[upos=NOUN|PRON|PROPN] }
    without { DEP2[upos=ADJ]; DEP2 -[det|nmod:poss]-> * }
    without { GOV -[dep.coord|S:dep.coord]-> DEP; DEP << DEP1 }
    commands {
      del_edge depcoord_rel;
      add_edge DEP1 -[orphan]-> DEP2
    }
  }

rule prep_prep{
    pattern {
      rel_coord: C1 -[coord|S:coord]-> GOV;
      DEP1[upos=ADP]; GOV -[dep.coord|S:dep.coord]-> DEP1;
      DEP2[upos=ADP];
      depcoord_rel:GOV -[dep.coord|S:dep.coord]-> DEP2; DEP1 << DEP2;
    }
    without { GOV -[dep.coord|S:dep.coord]-> DEP; DEP << DEP1 }
    commands {
      del_edge depcoord_rel;
      add_edge DEP1 -[orphan]-> DEP2
    }
  }

}

% =============================================================================================
% Embedded coordinations
package emb_coord{
  rule emb_init_coord{
    pattern{
      d1: C1 -[coord|S:coord]-> CC1;
      C1 -[coord]-> CC2;
      d2:C1 -[^coord]-> D;
      CC1 << D; D << CC2}
    without{d2.label=re"D:.*"}
    commands{ d1.2=emb}
  }

  rule emb_final_coord{
    pattern{
      d1: C1 -[coord|S:coord]-> CC1;
      CC1 -[dep.coord]-> C2;
      d2:C2 -[coord]-> CC2}
    commands{ d2.2=emb}
  }

}

% =============================================================================================
% Shared coordination dependencies
package shared_dep{
  rule left{
    pattern{
      C1 -[1=coord]-> CC2; CC2 -[dep.coord|S:dep.coord]-> C2;
      d1:C1 -> D; d2:C2 -> D; d1.label = d2.label;
      D << C1; D[!Shared]}
    without{C1[upos=ADJ]; d1.label="D:suj:suj"}
    commands{D.Shared=Yes}
  }
   
  rule right{
    pattern{
      coord_rel1:C1 -[1=coord]-> CC2; CC2 -[dep.coord|S:dep.coord]-> C2;
      d1:C1 -[^coord]-> D;
      C2 << D; D[!Shared]}
    without{
      coord_rel2:C1 -[1=coord]-> CC3; CC2 << CC3;
      coord_rel2.label=coord_rel1.label}
    without{d1.label=re"D:.*"}
    commands{
      D.Shared=Yes;
      add_edge d2:C2 -> D; d2.label =d1.label; del_edge d1}
  }

}
    
% =============================================================================================
% Transforming the distributive form of enumerations in Sequoia into the coordination form of SUD.
package coord {

% Enumeration of elements having the same function different from mod.
% Ex: Sequoia annodis.er_00019 - photos[G], réalisations, vieux outils[D1], documents[D2] anciens, permettront de mesurer
% Ex: Sequoia annodis.er_00140 - ils ont appris[G] à parler fort, à bien articuler, à[D1] ne pas tourner le dos au public,[COMMA] à[D1] s'amuser avec leur rôle.
  rule enum {
    pattern {
      d1: G -[^ S:dep|dep|dep_cpd|mod|ponct|udep]-> D1; d2: G -> D2; d1.label=d2.label;
      COMMA[upos=PUNCT,lemma=","];
      p: G -[S:ponct|ponct]-> COMMA;
      D1 << COMMA; COMMA << D2
    }
    without{* -[S:dep|mod]-> D2}
    without {d3: G -> D3; d1.label=d3.label; D2 << D3}
    without {d3: G -> D3; D1 << D3; D3 << D2; d3.label=re"S:.*"}
    commands {
      del_edge d2; del_edge p;
      add_edge D2 -[punct]-> COMMA; add_edge D1 -[conj]-> D2;
    }
  }

% Particular case of the previous rule.
% Ex: Sequoia annodis.er_00125 - des mots-clefs tels[G] : amas[D1] globulaire ,[COMMA] amas[D2] ouvert , comètes , constellations , galaxie , mama 
  rule enum_spec {
    pattern {
      d1: G -[^ S:dep|dep|dep_cpd|mod|ponct|udep]-> D1; d2: G -> D2; d1.label=d2.label;
      COMMA[upos=PUNCT,lemma=","];
      p: G -[S:ponct|ponct]-> COMMA;
      D1 << COMMA; COMMA << D2;
      d3: G -[S:dep|mod]-> D2
    }
    without {d4: G -> D3; d1.label=d4.label; D2 << D3}
    without {d4: G -> D3; D1 << D3; D3 << D2; d4.label=re"S:.*"}
    commands {
      del_edge d2; del_edge p; del_edge d3;
      add_edge D2 -[punct]-> COMMA; add_edge D1 -[conj]-> D2;
    }
  }

% % Enumeration of elements having the same function  mod and the same POS.
% % Counterex: Sequoia annodis.er_00028 - Elle occupait[G] depuis[D1] cette date, un bâtiment[D3] préfabriqué dans[D2] le parc Verneau.
%   rule enum_mod {
%     pattern {
%       d1: G -[mod]-> D1; d2: G -> D2;
%       d1.label=d2.label; D1.upos=D2.upos;
%       COMMA[upos=PUNCT,lemma=","];
%       p: G -[ponct]-> COMMA;
%       G << D1; D1 << COMMA; COMMA << D2
%     }
%     without {d3: G -> D3; d1.label=d3.label; D3.upos=D1.upos; D2 << D3}
%     without {d3: G -> D3; D1 << D3; D3 << D2}
%     commands {
%       del_edge d2; del_edge p;
%       add_edge D2 -[punct]-> COMMA; add_edge D1 -[conj]-> D2
%     }
%   }

% Precondition : a conjunct governs a coordinating conjunction via a COORD dependency, which governs a second conjunct.
% Action : the first conjunct governs the second conjunct directly.
  rule coord_conj {
    pattern {
      CONJ1[upos<>CCONJ]; CC[upos=CCONJ];
      coord_rel: CONJ1 -[S:coord|coord]-> CC;
      depcoord_rel:CC -[S:dep.coord|dep.coord]-> CONJ2}
    commands {
      add_edge CONJ1 -[conj]-> CONJ2;
      add_edge CONJ2 -[cc]-> CC;
      del_edge coord_rel;del_edge depcoord_rel}
  }

% Precondition : a conjunct governs an embedded coordinating conjunction via a COORD dependency, which governs a second conjunct.
% Action : the first conjunct governs the second conjunct directly.
  rule coord_emb_conj {
    pattern {
      CONJ1[upos<>CCONJ]; CC[upos=CCONJ];
      coord_rel: CONJ1 -[S:coord:emb|coord:emb]-> CC;
      depcoord_rel:CC -[S:dep.coord|dep.coord]-> CONJ2}
    commands {
      add_edge CONJ1 -[conj@emb]-> CONJ2;
      add_edge CONJ2 -[cc]-> CC;
      del_edge coord_rel;del_edge depcoord_rel}
  }

% Precondition : a conjunct governs a comma via a COORD dependency, which governs a second conjunct.
% Action : the first conjunct governs the second conjunct directly.
  rule coord_comma {
    pattern {
      CONJ1[upos<>CCONJ]; COMMA[upos=PUNCT,form=","|";"|"-"];
      coord_rel: CONJ1 -[S:coord|coord|S:coord:emb|coord:emb]-> COMMA;
      depcoord_rel:COMMA -[S:dep.coord|dep.coord]-> CONJ2}
    commands {
      add_edge CONJ1 -[conj]-> CONJ2;
      add_edge CONJ2 -[punct]-> COMMA;
      del_edge coord_rel;del_edge depcoord_rel}
  }

% Precondition : an embedded conjunct governs a comma via a COORD dependency, which governs a second conjunct.
% Action : the first conjunct governs the second conjunct directly.
  rule coord_emb_comma {
    pattern {
      CONJ1[upos<>CCONJ]; COMMA[upos=PUNCT,form=","|";"|"-"];
      coord_rel: CONJ1 -[S:coord:emb|coord:emb]-> COMMA;
      depcoord_rel:COMMA -[S:dep.coord|dep.coord]-> CONJ2}
    commands {
      add_edge CONJ1 -[conj@emb]-> CONJ2;
      add_edge CONJ2 -[punct]-> COMMA;
      del_edge coord_rel;del_edge depcoord_rel}
  }

% Precondition: two correlated coordinating conjunctions governs two conjuncts and the first conjunction governs the second conjunction.
% Action: the first conjunct becomes the governor of the two conjunctions and of the second conjunct.
  rule correl-coord {
    pattern {
      CC1[upos=CCONJ]; CONJ1[];
      depcoord_rel1: CC1 -[S:dep.coord|dep.coord]-> CONJ1;
      CC2[upos=CCONJ]; CONJ2[];
      coord_rel: CC1 -[S:coord|coord]-> CC2;
      depcoord_rel2:CC2 -[S:dep.coord|dep.coord]-> CONJ2}
    commands {
      shift CC1 =[1<>parseme|frsemcor]=> CONJ1;
      add_edge CONJ1 -[cc]-> CC1; add_edge CONJ2 -[cc]-> CC2;
      add_edge CONJ1 -[conj]-> CONJ2;
      del_edge depcoord_rel1; del_edge depcoord_rel2;  del_edge coord_rel;
    }
  }

% Precondition : a conjunction of coordination is not dependent of a first conjunct;  it governs a second conjunct in a DEP.COORD dependency.
% Action : add a MARK dependency from the head of the second conjunct to the conjunction.
 rule cut_coord {
    pattern{
      CC[upos=CCONJ];
      depcoord_rel: CC -[S:dep.coord|dep.coord]-> CONJ}
    without { * -[S:coord|coord|S:coord:emb|coord:emb|cc]-> CC }
    without { CC -[S:coord|coord|S:coord:emb|coord:emb]-> CC2; CC2[upos=CCONJ]; } % avoid conf convered by correl-coord
    commands {
      del_edge depcoord_rel;add_edge CONJ -[cc]-> CC;
      shift_in CC =[1<>parseme|frsemcor]=> CONJ;
      shift_out CC =[ponct]=> CONJ;
    }
  }
}

% =============================================================================================
% Transforming the bouquet form of coordinations into a chain
rule chain_coord{
  pattern{
    d1:C1 -[1=conj]-> C2;
    d2:C1 -[1=conj]-> C3;
    d1.label=d2.label;C2 << C3}
 without{C1 -[1=conj]-> C; C2 << C}
 commands{del_edge d2; add_edge C2 -[conj]-> C3}
}